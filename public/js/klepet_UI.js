/**
 * Funkcija, ki pregleda, kateri youtube posnetki so vključeni in
 * jih prikaže v pogovoru.
 */
function poisciYoutubePosnetke (vhodnoBesedilo) {
  vhodnoBesedilo = vhodnoBesedilo.split(" ");
  var posnetki = [];
  for (var i in vhodnoBesedilo) {
    if (vhodnoBesedilo[i].indexOf("https://www.youtube.com/watch?v=") != -1) {
      //vrne index, kjer se nahaja enačaj in nato shrani vse
      //kar se nahaja za enačajem, kar je ravno iskani videoposnetek
      var index = vhodnoBesedilo[i].indexOf("=");
      index++;
      var d = vhodnoBesedilo[i].length;
      if (vhodnoBesedilo[i].charAt(d-1) == '\"') {
        posnetki.push (vhodnoBesedilo[i].substring(index,d-1));
      }
      else {
        posnetki.push (vhodnoBesedilo[i].substring(index,d));
      }
    }
  }
  return posnetki;
}


/**
 * Funkcija, ki poisce vse omenjene v sporočilu in jih zapiše v tabelo
 */
function definirajOmenjene (vhodnoBesedilo) {
  vhodnoBesedilo = vhodnoBesedilo.split(" ");
  var omenjeni = [];
  for (var i in vhodnoBesedilo) {
    if (vhodnoBesedilo[i].charAt(0) == "@") {
      //če se nahaja na koncu niza zasebnega sporočila
      if (vhodnoBesedilo[i].slice(-1) == '"') {
        omenjeni.push (vhodnoBesedilo[i].slice(1,-1));
      }
      else {
        omenjeni.push (vhodnoBesedilo[i].slice(1));
      }
    } 
    //če se nahaja na začetku niza zasebnega sporočila
    else if (vhodnoBesedilo[i].charAt(0) == '"' &&
             vhodnoBesedilo[i].charAt(1) == "@" ) {
      //če je samo omenjena oseba v nizu in je na obeh straneh omejena z narekovaji
      if (vhodnoBesedilo[i].slice(-1) == '"') {
        vhodnoBesedilo[i] = vhodnoBesedilo[i].slice(2,-1);
        omenjeni.push (vhodnoBesedilo[i]);
      }
      else {
        omenjeni.push (vhodnoBesedilo[i].slice(2));
      }
    }
  }
  return omenjeni;
}


/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  var posnetki = poisciYoutubePosnetke(sporocilo);
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz({besedilo: sporocilo, posnetki: posnetki});
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  //preverimo, če se v sporočilu nahajajo omenjene osebe
  if (sporocilo.indexOf("@") != -1) {
    var omenjeni = definirajOmenjene (sporocilo);
    //vsem omenjenim posredujemo zasebno sporočilo omembe
    for (var i = 0; i < omenjeni.length; i++) {
      socket.emit ('sporocilo',{
        vzdevek: omenjeni[i], 
        besedilo: ("&#9758; Omemba v klepetu"),
        //določa da gre za pošiljanje sistemskega zasebnega sporočila
        sistemsko: true
      });
    }
  }
  
  //če so posnetki za prikazat trenutnemu uporabniku in je bilo sporočilo poslano pod zasebno
  //potem se prikaže videovsebina samo uporabniku in ne vsem na njegovem kanalu
  if (posnetki && sistemskoSporocilo) {
    klepetApp.prikaziYoutubePosnetke({posnetki: posnetki});
  } else {
    klepetApp.prikaziYoutubePosnetke({posnetki: posnetki, kanal: trenutniKanal});
  }
  
  $('#poslji-sporocilo').val('');
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  //Prikaži rezultat zahteve po spremembi barve
  socket.on('spremembaBarveOdgovor', function (barva) {
    $('#kanal').css('color', barva.barva);
    $('#sporocila').css('color', barva.barva);
  });
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  //Prikaži rezultat zahteve po prikazu videovsebine
  socket.on('posnetekOdgovor', function(posnetki) {
    //vsak posnetek prikažemo na odjemalcu
    for (var i in posnetki) {
      $('#sporocila')
      .append($('<div id="frame"></div>')
      .html('<iframe id="posnetek" src="https://www.youtube.com/embed/'+ posnetki[i] +'\" allowfullscreen></iframe>'));
    }
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var besedilo = sporocilo.besedilo;
    if (sporocilo.sistemsko) {
      besedilo = divElementHtmlTekst(sporocilo.besedilo);
    }
    else {
      besedilo = divElementEnostavniTekst(sporocilo.besedilo);
    }
    $('#sporocila').append(besedilo);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});



/* global $, io, Klepet */